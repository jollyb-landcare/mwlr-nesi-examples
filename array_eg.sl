#!/bin/bash
#SBATCH --job-name=my_job_name    # job name (shows up in the queue)
#SBATCH --account=landcareXXXXX   # Project Account
#SBATCH --time=12:00:00           # Walltime (HH:MM:SS)
#SBATCH --mem=1G                  # memory/cpu (in MB)
#SBATCH --partition=long          # specify a partition
#SBATCH --hint=nomultithread      # don't use hyperthreading
#SBATCH -o /nesi/nobackup/landcareXXXXX/logs/my_job_name.%A_%a.out
#SBATCH -e /nesi/nobackup/landcareXXXXX/logs/my_job_name.%A_%a.err
#SBATCH --array=1-1

date

module load Python/3.6.3-gimkl-2017a

echo "Make sure you set --array=x-y when you sbatch this job"

echo $SLURM_ARRAY_TASK_ID "ID"
# get the line from the text file that corresponds to our job ID
file_to_process=$(sed -n "${SLURM_ARRAY_TASK_ID}p" list.txt)

python my_script.py $file_to_process

date

